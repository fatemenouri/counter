import { TestBed } from '@angular/core/testing';

import { CounterFetchService } from './counter-fetch.service';

describe('CounterFetchService', () => {
  let service: CounterFetchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CounterFetchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
