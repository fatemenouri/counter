import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CounterFetchService {
  counter: number = 1000;
  subjectA: Subject<number> = new Subject();
  subCounter() {
      setTimeout(() => {
        this.counter=this.counter-1;
      this.subjectA.next(this.counter);
      this.subCounter();
    }, 1000);
    }
  
  addCounter(add:number){
    this.counter=this.counter+add;
  }
  constructor() { }
}
