import { Component, OnInit } from '@angular/core';
import { CounterFetchService } from 'src/app/services/counter-fetch.service';

@Component({
  selector: 'app-c2',
  templateUrl: './c2.component.html',
  styleUrls: ['./c2.component.scss']
})
export class C2Component implements OnInit {

  constructor(
    private counterFetchService:CounterFetchService
  ) { }

  ngOnInit(): void {
  }
  addcounter(){
    this.counterFetchService.addCounter(2);
  }

}
