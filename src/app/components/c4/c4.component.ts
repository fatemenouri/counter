import { Component, OnInit } from '@angular/core';
import { CounterFetchService } from 'src/app/services/counter-fetch.service';

@Component({
  selector: 'app-c4',
  templateUrl: './c4.component.html',
  styleUrls: ['./c4.component.scss']
})
export class C4Component implements OnInit {

  constructor(
    private counterFetchService:CounterFetchService
  ) { }

  ngOnInit(): void {
  }
  addcounter(){
    this.counterFetchService.addCounter(4);
  }

}
