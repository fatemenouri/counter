import { Component, OnInit } from '@angular/core';
import { CounterFetchService } from 'src/app/services/counter-fetch.service';
@Component({
  selector: 'app-c1',
  templateUrl: './c1.component.html',
  styleUrls: ['./c1.component.scss']
})
export class C1Component implements OnInit {
  sub:number=4;
  constructor(
    private counterFetchService:CounterFetchService
  ) { }
  
  
  ngOnInit(): void {
  }
  addcounter(){
    this.counterFetchService.addCounter(1);
  }
}
