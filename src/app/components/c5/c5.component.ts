import { Component, OnInit } from '@angular/core';
import { CounterFetchService } from 'src/app/services/counter-fetch.service';
@Component({
  selector: 'app-c5',
  templateUrl: './c5.component.html',
  styleUrls: ['./c5.component.scss']
})
export class C5Component implements OnInit {
  counter: number | undefined = this.counterFetchService.counter;
  constructor(
    private counterFetchService: CounterFetchService
  ) { }

  ngOnInit(): void {

    this.counterFetchService.subCounter();
    this.counterFetchService.addCounter(0);
    this.counterFetchService.subjectA.subscribe(
      data => {
        this.counter = data
      }
    )
  }

}
