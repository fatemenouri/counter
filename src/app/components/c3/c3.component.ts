import { Component, OnInit } from '@angular/core';
import { CounterFetchService } from 'src/app/services/counter-fetch.service';

@Component({
  selector: 'app-c3',
  templateUrl: './c3.component.html',
  styleUrls: ['./c3.component.scss']
})
export class C3Component implements OnInit {

  constructor(
    private counterFetchService:CounterFetchService
  ) { }

  ngOnInit(): void {
  }
  addcounter(){
    this.counterFetchService.addCounter(3);
  }

}
